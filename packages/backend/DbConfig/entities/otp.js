const getOTPEntity = (schema) => `id uuid DEFAULT uuid_generate_v4(),
            email text NOT NULL,
            code text NOT NULL,
            end_time integer NOT NULL,
            CONSTRAINT otps_pkey PRIMARY KEY (id),
            FOREIGN KEY (email) REFERENCES ${schema}."users"(email)`;

module.exports = {getOTPEntity};