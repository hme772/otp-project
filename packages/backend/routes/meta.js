const express = require('express');
const {getAllUserAndAppointmentTypes} = require("../controls/meta");
const validateGetAllUserAndAppointmentTypes = require("../validations/meta")
const validateResource = require('../middlewares/validateResource');

const router = express.Router();
router.get("/", validateResource(validateGetAllUserAndAppointmentTypes), getAllUserAndAppointmentTypes);

module.exports = router;