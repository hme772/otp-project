const express = require('express');
const auth = require('../middlewares/auth');
const {validateGetOTPByEmail, validateGenerateOTP} = require("../validations/otp");
const validateResource = require('../middlewares/validateResource');
const {getOTPByEmail, generateOTP} = require("../controls/otp");

const router = express.Router();
router.get("/", auth.protect, validateResource(validateGetOTPByEmail), getOTPByEmail);
router.post("/", auth.protect, validateResource(validateGenerateOTP), generateOTP);


module.exports = router;