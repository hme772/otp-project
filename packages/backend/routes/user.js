const express = require('express');
const {getAllUsers, getUserById, addUser, getIsUserExist, updateUser} = require('../controls/user');
const auth = require('../middlewares/auth');
const {
    validateGetIsUserExist,
    validateGetAllUsers,
    validateGetUserById,
    validateAddUser,
    validateUpdateUser,
} = require('../validations/user');
const validateResource = require('../middlewares/validateResource');

const router = express.Router();
router.get("/exist", validateResource(validateGetIsUserExist), getIsUserExist)
router.get("/", auth.protect, validateResource(validateGetAllUsers), getAllUsers);
router.get("/:userId", auth.protect, validateResource(validateGetUserById), getUserById);
router.post("/", auth.protect, validateResource(validateAddUser), addUser);
router.put("/:id", auth.protect, validateResource(validateUpdateUser), updateUser);


module.exports = router;