const admin = require("../config/firebase-config");

const protect = async (req, res, next) => {
    console.log(req.headers.authorization)

    try {
        if(!req.headers.authorization) throw new Error("no token recieved!")
        const idToken = req.headers.authorization.split(" ")[1];
        const decodedToken = await admin.auth().verifyIdToken(idToken);
        req.user = decodedToken;
        if(decodedToken) return next();
    } catch (err) {
        console.error(err)
        res.status(401).json({ error: 'Unauthorized' });
    }
};

module.exports = {protect};