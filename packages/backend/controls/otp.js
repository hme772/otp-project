const {generateOTPHandler, getOTPByEmailHandler} = require("../dataAccess/otp");
const {sendEmail} = require("../services/emailSender");

const getOTPByEmail = async (req, res) => {
    try {
        const result = await getOTPByEmailHandler({schema: req.body.schema, email: req.query.email});
        return res.json(result);
    } catch (err) {
        console.log(err);
        res.status(err.status || 500).send(err);
    }
};

const generateOTP = async (req, res) => {
    try {
        const {schema, email} = req.body;
        const result = await generateOTPHandler({schema, email});
        await sendEmail({otpCode: result.addedOtp.code, email,res});
        return res.json({message: `successfully generated ${result.rowCount}/1 OTP`, addedOtp: result.addedOtp});
    } catch (err) {
        console.log(err);
        res.status(err.status || 500).send(err);
    }
};

module.exports = {generateOTP,getOTPByEmail};