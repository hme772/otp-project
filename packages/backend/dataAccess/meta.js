const {getUserTypesQuery} = require('./sql/meta');
const globals = require("../config/globals");

const getAllUserAndAppointmentTypesHandler = async ({schema}) => {
    const userTypesQuery = getUserTypesQuery({schema});
    const userTypesResult = await globals.pg.client.query(userTypesQuery);
    return {userTypes: userTypesResult?.rows || []};
};

module.exports = {getAllUserAndAppointmentTypesHandler};