const getAllCities = async() => {
    let url = 'https://countriesnow.space/api/v0.1/countries';
    const res = await fetch(url);
    const data = await res.json()
    console.log(data)
    const citiesByCountries = data.data.map(country=>country.cities);
    const citiesarr = [];
    citiesByCountries.forEach(countryCities=>citiesarr.push(...countryCities));
    return citiesarr;
}

function getRandomCities(cities) {
    const shuffled = cities.sort(() => 0.5 - Math.random());
    return shuffled.slice(0, 3);
}

const getTemperatureByCity = async(cityName) => {
    let url =`http://api.weatherapi.com/v1/current.json?key=3870e26a4de144e3b68105714242005&q=${cityName}&aqi=no
    `;
    const res = await fetch(url);
    const data = await res.json();
    //console.log(data)
    return data;
}

const getCitiesTemperature = async (randomCities) => {
    const weatherPromises = randomCities.map(async (city) => {
        const cityData = await getTemperatureByCity(city);
        console.log(cityData.current)
        return cityData.current?.temp_c;
    });
    const weather = await Promise.all(weatherPromises);
    return weather;
}

const fixNum = (num) => {
    if(num<0) num*=-1;
    if(num<10) num = "0"+num
    return num;
}

const generateCode = (weatherArr) => {
    const initialValue = "";
    return weatherArr.reduce(
        (accumulator, currentValue) => accumulator + fixNum(Math.floor(currentValue)),
        initialValue,
    );
}

// Main function to fetch cities and get random ones
async function getOTPCode() {
    try {
        const cities = await getAllCities();
        const randomCities = getRandomCities(cities);
        console.log('Random Cities:', randomCities);
        const citiesWeather = await getCitiesTemperature(randomCities);
        console.log("citiesWeather",citiesWeather);
        const code = generateCode(citiesWeather);
        console.log("code", code)
        return code;
    } catch (error) {
        console.error('Error fetching cities:', error);
    }
}

module.exports = {getOTPCode, generateCode, getCitiesTemperature, getTemperatureByCity, getAllCities};