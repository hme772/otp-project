const getOTPsQuery = ({schema}) => `SELECT * FROM ${schema || 'otp'}.otps WHERE end_time > ${Math.floor(Date.now() / 1000)}`;
const getOTPByEmailQuery = ({schema}) => `${getOTPsQuery({schema})} AND email=$1;`;
const getGenerateOTPQuery = ({schema}) => `INSERT INTO 
        ${schema || 'OTP'}.otps (end_time, email, code )
        VALUES
        ($1, $2, $3) \nRETURNING *;`;

module.exports = {getOTPsQuery, getOTPByEmailQuery, getGenerateOTPQuery};