const getUserTypesQuery = ({schema}) => `SELECT * FROM ${schema || 'OTP'}.user_type;`;

module.exports = {getUserTypesQuery};
