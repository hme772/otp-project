const globals = require("../config/globals");
const {getOTPCode} = require("./codeGenerateService");
const {getGenerateOTPQuery, getOTPByEmailQuery} = require("./sql/otp");

const getOTPByEmailHandler = async ({schema, email}) => {
    const query = getOTPByEmailQuery({schema, email});
    const result = await globals.pg.client.query(query, [email]);
    return result?.rows || [];

};

const generateOTPHandler = async ({schema, email}) => {
    const fiveMinutesMS = 300000;
    const expiration_time = Math.floor((Date.now() + fiveMinutesMS) / 1000);
    const code = await getOTPCode();
    const values = [expiration_time, email, code];
    const query = getGenerateOTPQuery({schema});
    const result = await globals.pg.client.query(query, values);
    return {rowCount: result?.rowCount || 0, addedOtp: result?.rows[0]};
};
module.exports = {getOTPByEmailHandler,generateOTPHandler};