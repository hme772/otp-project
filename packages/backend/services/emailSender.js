const config = require("../config/config");
const sgMail = require("@sendgrid/mail");

sgMail.setApiKey(config.sendGrid.apiKey);


const sendEmail = async ({otpCode,email/*,res*/}) => {
    const msg1 = {
        to: email,
        from: config.sendGrid.fromEmail,
        subject: config.sendGrid.subject,
        text: `Your Code: ${otpCode}`,
        html: `<h1>Your Code:</h1><br/><b> ${otpCode}</b><br><p>This code is relevant for up to 5 minutes</p>`,
    };

    await sgMail.send(msg1);
}
module.exports = {sendEmail};
