const Joi = require("joi");
const {validationFunctionsObj} = require("./validationService");
const validateGenerateOTP = Joi.object({
    body: Joi.object({
        schema: validationFunctionsObj.schema,
        email: validationFunctionsObj.email,
    }),
    params: validationFunctionsObj.empty,
    query: validationFunctionsObj.empty,
});
const validateGetOTPByEmail = Joi.object({
    body: Joi.object({
        schema: validationFunctionsObj.schema,
    }),
    params: validationFunctionsObj.empty,
    query: Joi.object({
        email: validationFunctionsObj.email,
    }),
});
module.exports = {validateGenerateOTP, validateGetOTPByEmail};