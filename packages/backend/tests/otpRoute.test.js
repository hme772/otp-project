const request = require('supertest');
const express = require('express');
const auth = require('../middlewares/auth');
const { validateGetOTPByEmail, validateGenerateOTP } = require("../validations/otp");
const validateResource = require('../middlewares/validateResource');
const { getOTPByEmail, generateOTP } = require("../controls/otp");

const app = express();
app.use(express.json());

jest.mock('../middlewares/auth');
jest.mock('../validations/otp');
jest.mock('../middlewares/validateResource');
jest.mock('../controls/otp');

// Sample implementations for middleware and controllers to use in tests
auth.protect = jest.fn((req, res, next) => next());
validateResource.mockImplementation((validation) => (req, res, next) => next());
getOTPByEmail.mockImplementation((req, res) => res.status(200).json({
    id: "97b283db-32e2-4da8-b308-c22f05e66d98",
    email: "hodaya7720@gmail.com",
    code: "351517",
    end_time: 1716377600
}));
generateOTP.mockImplementation((req, res) => res.status(201).json({
    message: "successfully generated 1/1 OTP",
    addedOtp: {
        id: "bf36ebdf-e2c5-48b2-982f-f278328ebb77",
        email: "hodaya7720@gmail.com",
        code: "151725",
        end_time: 1716377734
    }
}));

const router = require('../routes/otp');
app.use('/otp', router);

describe('OTP Routes', () => {
    describe('GET /otp', () => {
        it('should get OTP by email if authorized and validated', async () => {
            const response = await request(app).get('/otp').set('Authorization', 'Bearer token');

            expect(response.status).toBe(200);
            expect(response.body).toEqual({
                id: "97b283db-32e2-4da8-b308-c22f05e66d98",
                email: "hodaya7720@gmail.com",
                code: "351517",
                end_time: 1716377600
            });
            expect(auth.protect).toHaveBeenCalled();
            expect(validateResource).toHaveBeenCalledWith(validateGetOTPByEmail);
            expect(getOTPByEmail).toHaveBeenCalled();
        });

        it('should return 401 if not authorized', async () => {
            auth.protect.mockImplementationOnce((req, res, next) => res.status(401).json({ error: 'Unauthorized' }));

            const response = await request(app).get('/otp');

            expect(response.status).toBe(401);
            expect(response.body).toEqual({ error: 'Unauthorized' });
        });
    });

    describe('POST /otp', () => {
        it('should generate OTP if authorized and validated', async () => {
            const response = await request(app).post('/otp').set('Authorization', 'Bearer token').send({ email: 'test@example.com' });

            expect(response.status).toBe(201);
            expect(response.body).toEqual({
                message: "successfully generated 1/1 OTP",
                addedOtp: {
                    id: "bf36ebdf-e2c5-48b2-982f-f278328ebb77",
                    email: "hodaya7720@gmail.com",
                    code: "151725",
                    end_time: 1716377734
                }
            });
            expect(auth.protect).toHaveBeenCalled();
            expect(validateResource).toHaveBeenCalledWith(validateGenerateOTP);
            expect(generateOTP).toHaveBeenCalled();
        });

        it('should return 401 if not authorized', async () => {
            auth.protect.mockImplementationOnce((req, res, next) => res.status(401).json({ error: 'Unauthorized' }));

            const response = await request(app).post('/otp').send({ email: 'test@example.com' });

            expect(response.status).toBe(401);
            expect(response.body).toEqual({ error: 'Unauthorized' });
        });
    });
});
