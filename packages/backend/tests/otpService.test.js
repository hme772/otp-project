const {getOTPCode, generateCode, getCitiesTemperature, getAllCities} = require('../dataAccess/codeGenerateService');
const fetchMock = require('jest-fetch-mock');

beforeEach(() => {
    fetchMock.resetMocks();
});

describe('getOTPCode', () => {

    test('should fetch cities, get random ones, fetch their weather, and generate a code', async () => {
        const code = await getOTPCode();

        expect(code).toMatch(/^(?=.*\b\d{6}\b).*$/);
    });
});

describe('getAllCities', () => {
    test('should return an array of cities', async () => {
        const cities = await getAllCities();
        expect(Array.isArray(cities)).toBe(true);
        expect(cities.length).toBeGreaterThan(0);
    });

    const originalFetch = global.fetch;

    afterEach(() => {
        global.fetch = originalFetch;
    });

    test('should throw an error if fetching fails', async () => {
        global.fetch = jest.fn(() =>
            Promise.reject(new Error('Failed to fetch'))
        );

        await expect(getAllCities()).rejects.toThrow('Failed to fetch');
    });
});

describe('getCitiesTemperature', () => {
    test('should return an array of temperatures for random cities', async () => {
        const randomCities = ['Eclectic', 'Saint-Jean-de-Soudain', 'Vaudesson'];
        const temperatures = await getCitiesTemperature(randomCities);
        expect(temperatures).toHaveLength(3);
        temperatures.forEach(temp => {
            expect(typeof temp).toBe('number');
            expect(temp).toBeGreaterThanOrEqual(-57);
            expect(temp).toBeLessThanOrEqual(57);
        });
    });

    test('should return array of undefineds if getTemperatureByCity fails for any city', async () => {
        const randomCities = ['a', 'b', 'c'];
        const temperatures = await getCitiesTemperature(randomCities);
        expect(temperatures).toHaveLength(3);
        const expectedResult = new Array(3).fill(undefined);
        expect(temperatures).toEqual(expectedResult);
    });
});

describe('generateCode', () => {
    test('should return a 6 digit code, containing only positive numbers', () => {
        const generateRandomNumber = () => {
            return (Math.random() * 2 - 1) * 57;
        }
        const randomWeathers = new Array(3).fill(generateRandomNumber());
        const code = generateCode(randomWeathers);
        expect(code).toMatch(/^(?=.*\b\d{6}\b).*$/);
    });

    test('if receives numbers of one digit should add leading 0, should return 000000 if all the temperatures are 0 ', () => {
        const randomWeathers = new Array(3).fill(0);
        const code = generateCode(randomWeathers);
        expect(code).toMatch('000000');
    });

    test('if received negative numbers should covert them to positives, should return a 6 digit code, containing only positive numbers', () => {
        const randomWeathers = [-1,0,-24];
        const code = generateCode(randomWeathers);
        expect(code).toMatch('010024');
    });

    test('if receives numbers of one digit should add leading 0, should return a 6 digit code, containing only positive numbers', () => {
        const randomWeathers = [3,5,7];
        const code = generateCode(randomWeathers);
        expect(code).toMatch('030507');
    });
});
