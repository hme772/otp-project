import axiosInstance, {getHeaders, routes} from "../axiosConfig";

export const getAllRelevantOTPsForEmail = async ({token,email}) => {
    try {
        return await axiosInstance.get(`${routes.otp}?email=${email}`, {headers: getHeaders(token)});
    } catch (e) {
        return false;
    }
};

export const generateOTP = async ({token, email}) => {
    try {
        return await axiosInstance.post(routes.otp, {
            schema: "otp",
            email,
        },{headers: getHeaders(token)});
    } catch (e) {
        return false;
    }
};