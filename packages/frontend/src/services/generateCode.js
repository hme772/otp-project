import {generateOTP, getAllRelevantOTPsForEmail} from "./server/otp";
import {slicedStore} from "../store/store";
import {setModalData} from "../store/reducers/users-reducer";

export const generateCode = async ({token, email}) => {
    try {
        const allRelevantCodes = await getAllRelevantOTPsForEmail({token,email});
        console.log(allRelevantCodes);
        if(allRelevantCodes.data.length === 0) {
            const newCode = await generateOTP({token,email});
            console.log(newCode);
            return newCode;
        }
        else {
            slicedStore.dispatch(setModalData({email}));
            return;
        }
    } catch (e) {
        return false;
    }
};

