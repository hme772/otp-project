import {confirmAlert} from "react-confirm-alert";
import {useEffect, useRef, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {getModalData} from "../../store/selectors/users-selector";
import {setModalData, deleteUser} from "../../store/reducers/users-reducer";
import classes from "./ActionModal.module.css";
import Button from "@mui/material/Button";
import CloseIcon from '@mui/icons-material/Close';
import {deleteUserInDb} from "../../services/server/user";
import LoadingSpinner from "../UI/LoadingSpinner";
import Details from "../Details/Details";
import store from "../../store/store";
import {Provider} from "react-redux";
import {generateOTP} from "../../services/server/otp";

const emptyFunction = () => {
};
const deleteTitle = "Delete User";
const deleteMessage = (name) => `Are You sure you want to delete ${name}?`;
const deleteNote = "All future OTP for this user will be deleted as well";
const CodeRegenerateTitle = "Regenerate Code";
const CodeRegenerateMessage = (email) => `Are You sure you want to regenerate a code for ${email}?`;
const CodeRegenerateNote = "There's already an available code for the entered email";

const ActionModal = ({token, navigate}) => {
    const modalData = useSelector(getModalData);
    const onCloseFunc = useRef(emptyFunction);
    const dispatch = useDispatch();
    const [isLoading, setIsLoading] = useState(false);

    const onDeleteUser = async () => {
        try {
            setIsLoading(true);
            const result = await deleteUserInDb({token, id: modalData.id});
            if (result.status === 200) {
                dispatch(deleteUser(modalData.id));
                alert("successfully deleted user!");
            }
            setIsLoading(false)
            onClose();
        } catch (err) {
            alert(err.message);
            setIsLoading(false);
        }

    };

    const onRegenerateCode = async () => {
        try {
            setIsLoading(true);
            const result = await generateOTP({token, email: modalData.email});
            if (result.status === 200) {
                alert('Check your mailbox for your new code');
                setIsLoading(false)
                onClose();
                if(navigate) navigate('/',{replace: true});
            }
            setIsLoading(false)
            onClose();
        } catch (err) {
            alert(err.message);
            setIsLoading(false);
        }
    };

    const onClose = () => {
        onCloseFunc.current();
        dispatch(setModalData(undefined));
    }


    useEffect(() => {
        // if onClose then call it and reset it
        if (onCloseFunc.current !== emptyFunction) {
            onClose()
            onCloseFunc.current = emptyFunction;
        }
        //const isShowModal = status !== 1 && (text || title);
        const isShowContent = modalData !== undefined;
        if (isShowContent) {
            confirmDialog();
        }
    }, [modalData]);

    const Dialog = () => {
        return <div className={classes.Dialog}>{isLoading && (
            <div className={modalData?.isDelete || modalData?.email ? classes.LoadingCentered : "centered"}>
                <LoadingSpinner/>
            </div>)}</div>
    }

    const fillContent = () => {
        const isDeleteOrEditShow =  (modalData?.isDelete && modalData?.id && modalData?.name) || (modalData?.isEdit && modalData?.id);
        const isRegenerateCodeShow = !modalData?.isDelete && !modalData?.isEdit && modalData?.email;
        if (!isDeleteOrEditShow && !isRegenerateCodeShow) return;
        if (modalData?.isDelete) {
            return <div className={classes.DialogContent}>
                <div className={classes.Rectangle}>
                    <Button onClick={onClose} className={classes.closeButton}>
                        <CloseIcon/>
                    </Button>
                    <h1>{deleteTitle}</h1>
                    <p>{deleteMessage(modalData?.name)}</p>
                    <p>{deleteNote}</p>
                    <Button className={classes.customButton} onClick={onDeleteUser}>Delete</Button>
                </div>
            </div>
        }
        else if(modalData?.isEdit) {
            return <Provider store={store}>
                <div className={classes.DialogContent}>
                    <div className={classes.RectangleDark}>
                        <Button onClick={onClose} className={classes.closeButton}>
                            <CloseIcon/>
                        </Button><Details id={modalData?.id} token={token}/></div>
                </div>
            </Provider>;
        }
        else {
            return <div className={classes.DialogContent}>
                <div className={classes.Rectangle}>
                    <Button onClick={onClose} className={classes.closeButton}>
                        <CloseIcon/>
                    </Button>
                    <h1>{CodeRegenerateTitle}</h1>
                    <p>{CodeRegenerateNote}</p>
                    <p>{CodeRegenerateMessage(modalData?.email)}</p>
                    <Button className={classes.customButton} onClick={onRegenerateCode}>Generate new Code</Button>
                </div>
            </div>
        }

    };

    const confirmDialog = () => {
        confirmAlert({
            closeOnEscape: false,
            closeOnClickOutside: false,
            customUI: ({onClose}) => {
                onCloseFunc.current = onClose;
                return (
                    <div className={classes.backdrop}>
                        {fillContent()}
                    </div>

                );
            },
        });
    };

    return modalData !== undefined ? <Dialog/> : <></>;

};

export default ActionModal;