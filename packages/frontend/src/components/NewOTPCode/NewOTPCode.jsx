import NewOTPCodeForm from './NewOTPCodeForm';
import classes from './NewOTPCode.module.css';

const NewOTPCode = () => {
  return (
    <section className={classes.newOtpCode}>
      <h1>New Code</h1>
      <NewOTPCodeForm />
    </section>
  );
};

export default NewOTPCode;
