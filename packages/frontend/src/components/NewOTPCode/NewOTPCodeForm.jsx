import {useRef, useContext, useState} from 'react';
import { useNavigate } from 'react-router-dom';
import AuthContext from '../../store/auth-context/auth-context';
import classes from './NewOTPCodeForm.module.css';
import {generateCode} from "../../services/generateCode";
import LoadingSpinner from "../UI/LoadingSpinner";

const NewOTPCodeForm = () => {
  const navigate = useNavigate();
  const emailRef = useRef();
  const authCtx = useContext(AuthContext);
  const [isLoading, setIsLoading] = useState(false);

  const submit = async (event) => {
    event.preventDefault();
    const email = emailRef.current?.value;
    try {
        setIsLoading(true);
        const result = await generateCode({token: authCtx.token, email});

        setIsLoading(false);
        if(result?.status === 200) {
            alert('Check your mailbox for your new code');
            navigate('/',{replace: true});
        }
    }
    catch (err) {
        alert(err.message);
        setIsLoading(false);
    }
  }

  return (
    <form className={classes.form} onSubmit={submit}>
      <div className={classes.control}>
        <label htmlFor='email'>Your Email</label>
        <input type='email' id='email' ref={emailRef} required/>
      </div>
        {isLoading && (
            <div className="centered">
                <LoadingSpinner />
            </div>
        )}
      <div className={classes.action}>
        <button>Generate new Code</button>
      </div>
    </form>
  );
}

export default NewOTPCodeForm;
