import { useState, useRef, useContext } from "react";
import { useNavigate } from "react-router-dom";
import LoadingSpinner from "../UI/LoadingSpinner";
import classes from "./AuthForm.module.css";
import AuthContext from "../../store/auth-context/auth-context";
import {getIsUserExist, getUserByEmail, registerUserToDb} from '../../services/server/user';
import {useDispatch, useSelector} from "react-redux";
import { getUserTypes } from "../../store/selectors/users-selector";
import {addUser} from "../../store/reducers/users-reducer";
import {evnObj} from "../../services/env";

const AuthForm = ({registerByWorker} ) => {
  const dispatch = useDispatch();
  const typeOptions = useSelector(getUserTypes) || [{id:1, type:'client'}]
  const navigate = useNavigate();
  const authCtx = useContext(AuthContext);
  const firstNameRef = useRef();
  const lastNameRef = useRef();
  const idRef = useRef();
  const phoneRef = useRef();
  const userTypeRef = useRef();

  const emailRef = useRef();
  const passwordRef = useRef();
  const [isLogin, setIsLogin] = useState(!authCtx.isLoggedIn || !registerByWorker);
  const [isLoading, setIsLoading] = useState(false);

  const switchAuthModeHandler = () => {
    setIsLogin((prevState) => !prevState);
  };

  const getToggle = () => {
    const isShowToggle = !registerByWorker;
    if( !isShowToggle) return;
    return isLogin ? "Create new account" : "Login with existing account";
  }

  const onSubmit = async (event) => {
    event.preventDefault();
  
    const email = emailRef.current?.value;
    const password = passwordRef.current?.value;
    const firstName = firstNameRef.current?.value;
    const lastName = lastNameRef.current?.value;
    const phone = phoneRef.current?.value;
    const userId = idRef.current?.value;
    const userType = userTypeRef.current?.value;

    setIsLoading(true);
    let url, isNeedRegistretion, isUserExist, isUserInDb, response;
    try {
      if (isLogin) {
        url =
          `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${evnObj.firebaseKey}`;
      } else {
        isNeedRegistretion = true;
        isUserExist = await getIsUserExist({id: userId, email});
        console.log("isUserExist",isUserExist)
        url =
          `https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=${evnObj.firebaseKey}`;
      }
      
      if(isLogin || !isUserExist){
       response = await fetch(url, {
        method: "POST",
        body: JSON.stringify({
          email,
          password,
          returnSecureToken: !registerByWorker,
        }),
        headers: {
          "Content-Type": "application/json",
        },
      });

      if (response.ok) {
        const data = await response.json();
        console.log("Success!", data);
       if(!isUserExist && isNeedRegistretion) {
         isUserInDb = await registerUserToDb({token: data.idToken,id: userId, firstName, lastName, phone, email, userType});
          console.log("isUserInDb",isUserInDb)
         dispatch(addUser(isUserInDb.data.addedUser))
       }

        setIsLoading(false);
        const expirationTimestamp = new Date(
          new Date().getTime() + parseInt(data.expiresIn) * 1000
        ).toISOString();
  
        const { id, first_name, last_name, user_type } = await getUserByEmail({token: data.idToken, email});
        !registerByWorker &&
          authCtx.login({
            token: data.idToken,
            expirationTime: expirationTimestamp,
            email,
            firstName: first_name,
            lastName: last_name,
            id,
            userType: user_type,
          });
        navigate("/", { replace: true });
      } else {
        const data = await response.json();
        let error = data?.error?.message ? data.error.message : "Authentication failed!";
        throw new Error(error);
      }}
    } catch (err) {
      alert(err.message);
      setIsLoading(false);
    }
  };
  

  return (
    <section className={classes.auth}>
      <h1>{isLogin && !registerByWorker ? "Login" : "Sign Up"}</h1>
      <form onSubmit={onSubmit}>
        {(!isLogin || (isLogin && registerByWorker)) && (
          <>
            <div className={classes.control}>
              <label htmlFor="firstName">First Name</label>
              <input type="text" id="firstName" required ref={firstNameRef} />
            </div>
            <div className={classes.control}>
              <label htmlFor="lastName">Last Name</label>
              <input type="text" id="lastName" required ref={lastNameRef} />
            </div>
            <div className={classes.control}>
              <label htmlFor="id">ID number</label>
              <input type="text" id="id" required ref={idRef} minLength={9}/>
            </div>
            <div className={classes.control}>
              <label htmlFor="phone">Phone Number</label>
              <input type="text" id="phone" required ref={phoneRef} minLength={10}/>
            </div>
            <div className={classes.control}>
              <label htmlFor="userType">User Type</label>
              <select
                name="userType"
                id="userType"
                defaultValue={typeOptions[0] || 1}
                required
                ref={userTypeRef}
              >
                {(!registerByWorker ? typeOptions.slice(0,1) : typeOptions).map((userType) => (
                  <option key={userType.id} value={userType.id}>{userType.type}</option>
                ))}
              </select>
            </div>
          </>
        )}
        <div className={classes.control}>
          <label htmlFor="email">Your Email</label>
          <input type="email" id="email" required ref={emailRef} />
        </div>
        <div className={classes.control}>
          <label htmlFor="password">Your Password</label>
          <input type="password" id="password" required ref={passwordRef} minLength={6} />
        </div>
        <div className={classes.actions}>
          {!isLoading && (
            <button>{isLogin && !registerByWorker ? "Login" : "Create Account"}</button>
          )}
          {isLoading && (
            <div className="centered">
              <LoadingSpinner />
            </div>
          )}
          <button
            type="button"
            className={classes.toggle}
            onClick={switchAuthModeHandler}
          >
           {getToggle()}
          </button>
        </div>
      </form>
    </section>
  );
};

export default AuthForm;
