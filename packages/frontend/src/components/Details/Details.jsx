import DetailsForm from './DetailsForm';
import classes from './Details.module.css';
import {useContext} from "react";
import {useSelector} from "react-redux";
import AuthContext from "../../store/auth-context/auth-context";
import {getCurrentUser} from "../../store/selectors/users-selector";

const Details = ({id, token}) => {
  const authCtx = useContext(AuthContext);
  const currentUser = useSelector(getCurrentUser(id || authCtx.id));
  const title = `${id ? "User" : "Your"} Details`
  return (
    <section className={classes.details}>
      <h1>{title} </h1>
        {currentUser ? <DetailsForm {...currentUser} token={token || authCtx.token}/> : <p>No User Found!</p> }
    </section>
  );
};

export default Details;
