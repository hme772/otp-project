import {useRef, useContext, useState, useEffect} from 'react';
import AuthContext from '../../store/auth-context/auth-context';
import classes from './DetailsForm.module.css';
import {useDispatch, useSelector} from "react-redux";
import {getIsWorkerUser, getUserTypes} from "../../store/selectors/users-selector";
import LoadingSpinner from "../UI/LoadingSpinner";
import {updateUserInDb} from "../../services/server/user";
import {updateUser} from "../../store/reducers/users-reducer";

const DetailsForm = ({token, id, first_name, last_name, phone_number, email, user_type}) => {
    const authCtx = useContext(AuthContext);
    const dispatch = useDispatch();
    const typeOptions = useSelector(getUserTypes) || [{id: 1, type: 'client'}];
    const isWorker = useSelector(getIsWorkerUser(user_type));
    const firstNameRef = useRef();
    const lastNameRef = useRef();
    const idRef = useRef();
    const phoneRef = useRef();
    const userTypeRef = useRef();
    const emailRef = useRef();
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        idRef.current.value = id
        firstNameRef.current.value = first_name;
        lastNameRef.current.value = last_name;
        phoneRef.current.value = phone_number;
        userTypeRef.current.value = user_type;
        emailRef.current.value = email;
    }, []);


    const submit = async event => {
        event.preventDefault();

        const email = emailRef.current?.value;
        const firstName = firstNameRef.current?.value;
        const lastName = lastNameRef.current?.value;
        const phone = phoneRef.current?.value;
        const userId = idRef.current?.value;
        const userType = userTypeRef.current?.value;
        setIsLoading(true);
        try {
            const updatedUserResult = await updateUserInDb({
                token: token,
                id: userId,
                email,
                first_name: firstName,
                last_name: lastName,
                phone_number: phone,
                user_type: userType
            });
            if (updatedUserResult.status === 200) {
                console.log(updatedUserResult)
                dispatch(updateUser(updatedUserResult.data.updatedUser));
                if (authCtx.id == userId) {
                    authCtx.updateConnectedUser({
                        first_name: firstName,
                        last_name: lastName,
                        user_type: userType
                    })
                }
                alert("successfully updated user!");
                setIsLoading(false);
            }
        } catch (err) {
            alert(`failed to update user.\n error: ${err.response.data.error }`);
            setIsLoading(false)
        }
    }

    return (
        <form className={classes.form} onSubmit={submit}>
            <div className={classes.control}>
                <label htmlFor="id">ID number</label>
                <input type="text" id="id" required ref={idRef} minLength={9} maxLength={9} disabled={true}/>
            </div>
            <div className={classes.control}>
                <label htmlFor="firstName">First Name</label>
                <input type="text" id="firstName" required ref={firstNameRef}/>
            </div>
            <div className={classes.control}>
                <label htmlFor="lastName">Last Name</label>
                <input type="text" id="lastName" required ref={lastNameRef}/>
            </div>
            <div className={classes.control}>
                <label htmlFor="phone">Phone Number</label>
                <input type="text" id="phone"
                       inputMode="numeric"
                       pattern="((\d{3}-)?\d{7})|([0-9\s]{10})"
                       required ref={phoneRef} minLength={10} maxLength={11}/>
            </div>
            <div className={classes.control}>
                <label htmlFor="userType">User Type</label>
                <select
                    name="userType"
                    id="userType"
                    defaultValue={typeOptions[0] || 1}
                    required
                    ref={userTypeRef}
                >
                    {(!isWorker ? typeOptions.slice(0, 1) : typeOptions).map((userType) => (
                        <option key={userType.id} value={userType.id}>{userType.type}</option>
                    ))}
                </select>
            </div>
            <div className={classes.control}>
                <label htmlFor="email">Your Email</label>
                <input type="email" id="email" required ref={emailRef} disabled={true}/>
            </div>
            {isLoading && (
                <div className="centered">
                    <LoadingSpinner/>
                </div>
            )}

            <div className={classes.action}>
                <button>Submit</button>
            </div>
        </form>
    );
}

export default DetailsForm;
